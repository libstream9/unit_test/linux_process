#include <stream9/linux/fork.hpp>

#include <stream9/linux/wait.hpp>

#include "namespace.hpp"

#include <unistd.h>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(fork_)

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        try {
            ::pid_t child;

            {
                auto pid = lx::fork();
                if (pid == 0) { // child
                    ::_exit(0);
                }
                else { // parent
                    child = pid;
                }
            }

            auto rc = ::waitpid(child, nullptr, 0);
            BOOST_REQUIRE(rc == -1);
            BOOST_TEST(errno == ECHILD);
        }
        catch (...) {
            st9::print_error();
        }
    }

BOOST_AUTO_TEST_SUITE_END() // fork_

} // namespace testing
