#ifndef STREAM9_LINUX_PROCESS_TEST_SRC_NAMESPACE_HPP
#define STREAM9_LINUX_PROCESS_TEST_SRC_NAMESPACE_HPP

namespace stream9::linux {}
namespace stream9::strings {}

namespace testing {

namespace st9 { using namespace stream9; }
namespace lx { using namespace stream9::linux; }
namespace str { using namespace stream9::strings; }

} // namespace testing

#endif // STREAM9_LINUX_PROCESS_TEST_SRC_NAMESPACE_HPP
