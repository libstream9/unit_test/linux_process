#include <stream9/linux/process/pipe.hpp>

#include "namespace.hpp"

#include <unistd.h>

#include <boost/test/unit_test.hpp>

#include <stream9/linux/read.hpp>
#include <stream9/linux/write.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(pipe_)

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        auto [r, w] = lx::pipe();

        auto n = lx::write(w, "foo").or_throw();
        BOOST_TEST(n == 4);

        char buf[10] {};
        auto m = lx::read(r, buf).or_throw();
        std::span rv { buf, m };

        BOOST_TEST(rv.size() == 4);
        BOOST_TEST(rv.data() == "foo");
    }

    BOOST_AUTO_TEST_CASE(with_flags_)
    {
        auto [r, w] = lx::pipe(O_CLOEXEC);

        auto n = lx::write(w, "foo").or_throw();
        BOOST_TEST(n == 4);

        char buf[10] {};
        auto m = lx::read(r, buf).or_throw();
        std::span rv { buf, m };

        BOOST_TEST(rv.size() == 4);
        BOOST_TEST(rv.data() == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // pipe_

} // namespace testing
