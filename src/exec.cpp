#include <stream9/linux/process/exec.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/args.hpp>

namespace testing {

using stream9::args;

BOOST_AUTO_TEST_SUITE(exec_)

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        args argv { "ppp" };

        BOOST_CHECK_EXCEPTION(
            lx::exec("ppp", argv),
            stream9::error,
            [](auto&& e) {
                BOOST_TEST(e.why() == lx::enoent);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(case_2_)
    {
        BOOST_CHECK_EXCEPTION(
            lx::exec("ppp", { "ppp" }),
            stream9::error,
            [](auto&& e) {
                BOOST_TEST(e.why() == lx::enoent);
                return true;
            } );
    }

    BOOST_AUTO_TEST_CASE(case_3_)
    {
        std::vector<char const*> argv { "foo", "bar" };

        BOOST_CHECK_EXCEPTION(
            lx::exec("ppp", argv),
            stream9::error,
            [](auto&& e) {
                BOOST_TEST(e.why() == lx::enoent);
                return true;
            } );
    }

BOOST_AUTO_TEST_SUITE_END() // exec_

} // namespace testing
